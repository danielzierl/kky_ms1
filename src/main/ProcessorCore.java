
package src.main;
import javaSimulation.Process;
import src.main.LinuxProcess;
import javaSimulation.Head;
public class ProcessorCore extends Process {

    Head processQueue;
    Head freeCores;
    int coreID;
    double scheduler_period;
    public ProcessorCore(int coreID,Head processQueue, Head freeCores, double scheduler_period){
        
        super();
        this.processQueue = processQueue;
        this.freeCores = freeCores;
        this.coreID = coreID;
        this.scheduler_period = scheduler_period;
        System.out.println("Processor Core(id: "+coreID+") created");
    }
    public void actions(){
            while(true){

                // leave the free cores list
                out(); 
                // do the calculations
                while(!processQueue.empty()){
                    LinuxProcess process = (LinuxProcess) processQueue.first();
                    int queue_len = processQueue.cardinal();
                    // the priority over normal process with niceness 0
                    double process_weight = process.getProcessWeight();
                    double total_process_weight = getProcessQueueTotalProcessWeight(processQueue);
                    // System.out.println("Core(id: "+coreID+") is working on process: "+process.processID+", queue size: "+processQueue.cardinal()+"free cores:"+freeCores.cardinal());
                    process.out();

                    double process_designated_time = (process_weight/total_process_weight)*scheduler_period;
                    double process_remaining_time = process.execution_time - process.time_spent_executing;
                    // if the process will finish
                    if(process_remaining_time < process_designated_time){
                        process.time_spent_executing += process_remaining_time;
                        hold(process_remaining_time);
                        activate(process);
                    }else{
                        process.time_spent_executing += process_designated_time;
                        hold(process_designated_time);
                        process.into(processQueue);
                    }

                    if(!processQueue.empty()){
                        // System.out.println("Core(id: "+coreID+") continues with a process from queue of size: "+processQueue.cardinal());
                    }
                }
                wait(freeCores);
                // System.out.println("Time: "+time()+"Core(id: "+coreID+") is idle, freeCores: "+freeCores.cardinal());
            }
        }
    public double getProcessQueueTotalProcessWeight(Head processQueue){
       LinuxProcess first = (LinuxProcess) processQueue.first();
       double total_process_weight = 0;
       while(first != null){
           total_process_weight += first.getProcessWeight();
           first = (LinuxProcess) first.suc();
       }
       return total_process_weight;

    }

}
