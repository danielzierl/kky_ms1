
package src.main;
import javaSimulation.Process;
import javaSimulation.Random;
import src.main.ProcessorCore;
import javaSimulation.Head;
public class LinuxProcess extends Process{
    double niceness;
    double execution_time;
    double time_spent_executing;
    int processID;
    Random random;
    Head processQueue;
    Head freeCores;
    public LinuxProcess(int processID,Head processQueue, Head freeCores){
        super();
        this.processQueue = processQueue;
        this.freeCores = freeCores;
        this.processID = processID;

        random = new Random(processID*1000+1);
        execution_time = random.negexp(1/40.0);


        niceness = random.normal(0, 10);
        niceness = Math.floor(niceness);
        // clamp it
        niceness = Math.max(niceness, -20);
        niceness = Math.min(niceness, 19);

    }
    public void actions(){
        String t = Double.toString(time());
        System.out.println("Time: "+t+" Linux process (id: "+processID+") activated, free cores: "+freeCores.cardinal()+" , exec time: "+execution_time+ " niceness: "+niceness);
        into(processQueue);
        if(!freeCores.empty()){
            System.out.println("trying to get a core");
            ProcessorCore p = (ProcessorCore)freeCores.first();
            System.out.println("got a core: "+p.coreID);
            activate(p);
        }
        passivate();
        // when gets called again to resume the process is finished
        System.out.println("Time: "+time()+" Linux process (id: "+processID+") finished, exec time: "+execution_time+" ,niceness: "+niceness);

 
    }
    public double getProcessWeight(){
        return (20-niceness)/20;
    }
}
