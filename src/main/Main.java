package src.main;
import javaSimulation.*;
import javaSimulation.Process;


public class Main{

    // all time is in ms
    static int SIMULATION_TIME = 1000;
    static int NUM_CORES = 2;
    static double SCHEDULER_PERIOD = 5;
    public static void main(String args[]) {
        SchedulerSimulation s1 = new SchedulerSimulation(SIMULATION_TIME, NUM_CORES, SCHEDULER_PERIOD);
        s1.start();
    } 


}
