
package src.main;
import javaSimulation.Process;
import javaSimulation.Random;
import javaSimulation.Head;

public class LinuxProcessGenerator extends Process{
    int simulationTime;
    Random random;
    Head processQueue;
    Head freeCores;
    int processes_generated = 0;
    public LinuxProcessGenerator(int simulationTime, Head processQueue, Head freeCores){
        super();
        this.freeCores = freeCores;
        this.processQueue = processQueue;
        this.simulationTime = simulationTime;
        random = new Random(1);
        
    }
    public void actions(){
        while( time() < this.simulationTime ){
            int processID = processes_generated++;
            activate(new LinuxProcess(processID,processQueue, freeCores));
            hold(random.negexp(1/5.0));
        }
    }
}
