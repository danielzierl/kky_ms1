package src.main;
import javaSimulation.Process;

import java.util.stream.IntStream;

import javaSimulation.Head;

public class SchedulerSimulation extends Process{
    int simulationTime;
    double startTime;
    Head processQueue;
    Head freeCores;
    int num_cores; 
    double scheduler_period;
    SchedulerSimulation(int simulationTime, int num_cores, double scheduler_period){

        this.simulationTime = simulationTime;
        this.processQueue = new Head();
        this.freeCores = new Head();
        this.num_cores = num_cores;
        IntStream.range(0, num_cores).forEach(i -> {
            new ProcessorCore(i,processQueue, freeCores, scheduler_period).into(freeCores);
        });

    }
    void start(){
        startTime = time();
        activate(this);
    }
    public void actions(){
        activate(new LinuxProcessGenerator(this.simulationTime, processQueue, freeCores));
        hold(simulationTime);
        while(!processQueue.empty() || freeCores.cardinal() != num_cores){
            hold(1);
        }
        report();
    }
    public void report(){
        System.out.println("Execution time (ms): "+(time() - startTime));

    }
    
}
